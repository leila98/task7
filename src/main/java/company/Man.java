package company;

public class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Man() {
    }

    @Override
    public String greetPet(Pet pet) {
        return super.greetPet(pet);
    }

    @Override
    public String describePet(Pet pet) {
        return super.describePet(pet);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar() {
        System.out.println("He will repair his car");
    }

    ;
}
//

//}
