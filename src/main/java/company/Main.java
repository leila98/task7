package company;

public class Main {

    public static void main(String[] args) {


        String[] habits = {"habit-1", "habit-2"};

        Pet pet1 = new Pet("toplan", 12, 32, habits) {
            @Override
            public void respond() {
                System.out.println("salam");
            }
        };

//pet1.setSpecies(Species.DOG);

        Human mother1 = new Human("khatira", "hasanova", 1965);


        Human father1 = new Human("arzu", "mammadov", 1964);
        String[] habits1 = {"eat", "sleep"};
        int[][] schedule1 = new int[7][1];
        String arr1[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to school"},
                {String.valueOf(DayOftheWeek.Tuesday), "Read a book"},
                {String.valueOf(DayOftheWeek.Wednesday), "Buy the book"},
                {String.valueOf(DayOftheWeek.Thursday), "Drink a coffee"},
                {String.valueOf(DayOftheWeek.Friday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Saturday), "Go to store"},
                {String.valueOf(DayOftheWeek.Sunday), "Buy furniture"}};


        Human child1 = new Human("leila", "mammadova", 1998, 88, arr1);


        String arr2[][] = {{"Monday", "Go to the gym"},
                {"Tuesday", "Write a code"},
                {"Wednesday", "Do delivering"},
                {"Thursday", "Do homework"},
                {"Friday", "Playing football"},
                {"Saturday", "Meet with friends"},
                {"Sunday", "Go to breakfast"}};


        Human child2 = new Human("anar", "rzayev", 2007, 43, arr1);
        Family family = new Family(father1, new Human[]{child1, child2}, mother1, pet1);

        Family family2 = new Family(father1, new Human[]{child1, child2}, family, mother1, pet1);
        System.out.println(family2);
    }

}
//    public static void main(String[] args){
//
//        Human mother1 = new Human("khatira", "hasanova", 1965) {
//            @Override
//            public String greetPet() {
//                return "greetpet-1";
//            }
//        };
//        Human father1 = new Human("arzu", "mammadov", 1964) {
//            @Override
//            public String greetPet() {
//                return "greetpet-2";            }
//        };
//        String[] habits1 = {"eat", "sleep"};
//        Pet pet1 = new Pet("toplan",4, 78, habits1,Species.Dog) {
//            @Override
//            public void eat() {
//                System.out.println("pet1-eating");
//            }
//
//            @Override
//            public void respond() {
//                System.out.println("pet1-responding");
//
//            }
//        };
//        int[][] schedule1 = new int[7][1];
//        String arr1[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to school"},
//                {String.valueOf(DayOftheWeek.Tuesday), "Read a book"},
//                {String.valueOf(DayOftheWeek.Wednesday), "Buy the book"},
//                {String.valueOf(DayOftheWeek.Thursday), "Drink a coffee"},
//                {String.valueOf(DayOftheWeek.Friday), "Meet with friends"},
//                {String.valueOf(DayOftheWeek.Saturday), "Go to store"},
//                {String.valueOf(DayOftheWeek.Sunday), "Buy furniture"}};
//
//
//        Human child1 = new Human("leila", "mammadova", 1998, 88, pet1, mother1, father1, arr1) {
//            @Override
//            public String greetPet() {
//              return "child1-greetPet";
//
//            }
//        };
//
//
//        String arr2[][] = {{"Monday", "Go to the gym"},
//                {"Tuesday", "Write a code"},
//                {"Wednesday", "Do delivering"},
//                {"Thursday", "Do homework"},
//                {"Friday", "Playing football"},
//                {"Saturday", "Meet with friends"},
//                {"Sunday", "Go to breakfast"}};
//
//
//        Human child2 = new Human("anar", "rzayev", 2007, 43, pet1, mother1, father1, arr2) {
//            @Override
//            public String greetPet() {
//                return "child2-greetPet";
//            }
//        };
//        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, pet1, mother1, father1, arr2) {
//            @Override
//            public String greetPet() {
//                return "child3-greetPet";
//
//            }
//        };
//
//        Family family = new Family(mother1, father1, new Human[]{child1, child2});
//
//
//        family.addChild(child3);
//
//        System.out.println(family.countFamily());
//        boolean any = family.deleteChild2(1);
//        System.out.println(any);
//    }}