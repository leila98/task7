package company;

import java.util.Arrays;

import static company.Species.*;

public class Family {


    public Family(Human father, Human[] children, Family family, Human mother, Pet pet) {
        this.father = father;
        this.children = children;
        this.family = family;
        this.pet = pet;
        this.mother = mother;
    }


    public Family(Human father, Human[] children, Human mother, Pet pet) {
        this.father = father;
        this.children = children;
        this.pet = pet;
        this.mother = mother;
    }


    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    private Human father;

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }
//
//    @Override
//    public String describePet() {
//        return super.describePet();
//    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private Human[] children;
    private Family family;
    private Pet pet;
    private Human mother;

    //    @Override
//    public String greetPet() {
//        return "null";
//    }


    public boolean deleteChild(Human human) {
        Human[] childrenArray = new Human[getChildren().length - 1];
        boolean delete = true;
        int index = 0;

        for (Human child : getChildren()) {
            if (!child.equals(human)) {
                childrenArray[index++] = child;
            }
        }
        this.children = childrenArray;
        if (index < childrenArray.length - 1) {
            delete = true;
        }
        return delete;
    }


    public boolean deleteChild2(int index) {
        Human[] childrenArray = new Human[getChildren().length - 1];
        boolean delete = true;
        int index2 = 0;
        for (Human child : getChildren()) {
            if (child != getChildren()[index]) {
                childrenArray[index2++] = child;
            }
        }
        this.children = childrenArray;
        if (index2 < childrenArray.length - 1) {
            delete = true;
        }
        return delete;
    }


    public void addChild(Human human) {
        Human[] childrenArray = new Human[children.length + 1];
        for (int a = 0; a < children.length; a++) {
            childrenArray[a] = children[a];
        }
        childrenArray[getChildren().length] = human;
        this.children = childrenArray;
    }


    public String countFamily() {
        int members = children.length + 2;
        return "Count of members in the family:".concat(String.valueOf(members));
    }

    @Override
    public String toString() {
        return "Family{" +
                "father=" + getFather() +
                ", children=" + Arrays.toString(children) +
                ", family=" + getFamily() +
                ", pet=" + getPet() +
                ", mother=" + getMother() +
                '}';
    }
}
